document.addEventListener("DOMContentLoaded", (event) => {
  const grid = document.querySelector(".grid");
  const scoreBar = document.getElementById("score");
  const width = 28; // 28x28 square
  const mazeRow = 28;
  const mazeCol = 28;
  let score = 0;
  const layout = [
    [
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 3, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 3, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 1, 1, 1, 2, 2, 1, 1, 1, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 1, 2, 2, 2, 2, 2, 2, 1, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      4, 4, 4, 4, 4, 4, 0, 0, 0, 4, 1, 2, 2, 2, 2, 2, 2, 1, 4, 0, 0, 0, 4, 4, 4,
      4, 4, 4,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 1, 2, 2, 2, 2, 2, 2, 1, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 0, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 0, 1, 1, 1,
      1, 1, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 3, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0,
      0, 3, 1,
    ],
    [
      1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0,
      1, 1, 1,
    ],
    [
      1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0,
      1, 1, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 0, 1,
    ],
    [
      1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1,
    ],
    [
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1,
    ],
  ];
  //0 - pacman-dot
  //1 - wall
  // 2 - ghost-space
  // 3 - power-pellet
  // 4 - empty

  const squares = [];

  function createMap() {
    for (let i = 0; i < mazeRow; i++) {
      for (let j = 0; j < mazeCol; j++) {
        const square = document.createElement("div");

        //Math.ceil(1/9) -1
        // x - row*9

        //add layout to map
        if (layout[i][j] === 0) {
          square.classList.add("pac-dot");
          square.classList.add(`[${i}][${j}]`);
          grid.appendChild(square);
          squares.push(square);
        } else if (layout[i][j] === 1) {
          square.classList.add("wall");
          square.classList.add(`[${i}][${j}]`);
          grid.appendChild(square);
          squares.push(square);
        } else if (layout[i][j] === 2) {
          square.classList.add("ghost-space");
          square.classList.add(`[${i}][${j}]`);
          grid.appendChild(square);
          squares.push(square);
        } else if (layout[i][j] === 3) {
          square.classList.add("power-pellet");
          square.classList.add(`[${i}][${j}]`);
          grid.appendChild(square);
          squares.push(square);
        } else {
          square.classList.add(`[${i}][${j}]`);
          grid.appendChild(square);
          squares.push(square);
        }
      }
    }
  }
  createMap();
  console.log(layout[10][9]);
  const to2DPosition = function (input) {
    let row = Math.floor(input / 28);
    let col = input - row * 28;
    return [row, col];
  };

  const to1DPosition = function (input1, input2) {
    return input1 * 28 + input2;
  };

  //create pac-man
  let pacmanCurrentPosistion = 490;
  squares[pacmanCurrentPosistion].classList.add("pac-man");

  //move pacman
  function movePacman(e) {
    squares[pacmanCurrentPosistion].classList.remove("pac-man");

    switch (e.keyCode) {
      case 37: //left
        if (
          pacmanCurrentPosistion % width !== 0 &&
          !squares[pacmanCurrentPosistion - 1].classList.contains("wall") &&
          !squares[pacmanCurrentPosistion - 1].classList.contains("ghost-space")
        ) {
          pacmanCurrentPosistion -= 1;
        }
        //check if pac man is existing the map
        if (squares[pacmanCurrentPosistion - 1] === squares[363]) {
          pacmanCurrentPosistion = 391;
        }
        break;
      case 38: //up
        if (
          pacmanCurrentPosistion - width >= 0 &&
          !squares[pacmanCurrentPosistion - width].classList.contains("wall") &&
          !squares[pacmanCurrentPosistion - width].classList.contains(
            "ghost-space"
          )
        ) {
          pacmanCurrentPosistion -= width;
        }
        break;
      case 39: //right
        if (
          pacmanCurrentPosistion % width < width - 1 &&
          !squares[pacmanCurrentPosistion + 1].classList.contains("wall") &&
          !squares[pacmanCurrentPosistion + 1].classList.contains("ghost-space")
        ) {
          pacmanCurrentPosistion += 1;
        }
        //check if pac man is existing the map
        if (squares[pacmanCurrentPosistion + 1] === squares[392]) {
          pacmanCurrentPosistion = 364;
        }
        break;
      case 40: //down
        if (
          pacmanCurrentPosistion + width < width * width &&
          !squares[pacmanCurrentPosistion + width].classList.contains("wall") &&
          !squares[pacmanCurrentPosistion + width].classList.contains(
            "ghost-space"
          )
        ) {
          pacmanCurrentPosistion += width;
        }
        break;
    }
    squares[pacmanCurrentPosistion].classList.add("pac-man");
    pacmanEatDot();
    eatPowerPellet();
    checkForGameOver();
    checkForWin();
  }

  document.addEventListener("keyup", movePacman);

  function pacmanEatDot() {
    if (squares[pacmanCurrentPosistion].classList.contains("pac-dot")) {
      score++;
      scoreBar.innerHTML = score;
      squares[pacmanCurrentPosistion].classList.remove("pac-dot");
    }
  }

  function eatPowerPellet() {
    if (squares[pacmanCurrentPosistion].classList.contains("power-pellet")) {
      score += 10;
      ghosts.forEach((ghost) => (ghost.isScared = true));
      specialGhost.isScared = true;
      setTimeout(unScareGhosts, 10000);
      squares[pacmanCurrentPosistion].classList.remove("power-pellet");
    }
  }

  function unScareGhosts() {
    ghosts.forEach((ghost) => (ghost.isScared = false));
    specialGhost.isScared = false;
  }

  class Ghost {
    constructor(className, startIndex, speed) {
      this.className = className;
      this.startIndex = startIndex;
      this.speed = speed;
      this.currentIndex = startIndex;
      this.isScared = false;
      this.timerId = NaN;
      this.scarePoint = null;
    }
  }

  ghosts = [
    new Ghost("blinky", 161, 250),
    new Ghost("pinky", 161, 200),
    new Ghost("inky", 161, 200),
    new Ghost("clyde", 161, 500),
  ];

  //special ghost chase pacman
  specialGhost = new Ghost("specialGhost", 289, 200);
  squares[specialGhost.currentIndex].classList.add("special-ghost");
  let dot = false;
  let dotPosition = null;

  function specialGhostMove(path) {
    clearInterval(specialGhost.timerId);
    let i = 0;
    specialGhost.timerId = setInterval(() => {
      if (i < path.length) {
        let addDot = () => {
          if (dot) {
            squares[dotPosition].classList.add("pac-dot");
            dot = false;
          }
        };
        addDot();
        checkScared();

        squares[specialGhost.currentIndex].classList.remove("scared-ghost");
        squares[specialGhost.currentIndex].classList.remove(
          "special-ghost",
          "ghost"
        );

        //special ghost moved
        let nextMove = to1DPosition(path[i][0], path[i][1]);

        if (squares[nextMove].classList.contains("ghost")) {
          specialGhost.currentIndex = specialGhost.currentIndex;
          checkScared();
        } else {
          specialGhost.currentIndex = nextMove;
          checkScared();
        }

        addDot();

        squares[specialGhost.currentIndex].classList.add(
          "special-ghost",
          "ghost"
        );

        if (specialGhost.currentIndex === specialGhost.scarePoint) {
          specialGhost.scarePoint = null;
        }

        if (squares[specialGhost.currentIndex].classList.contains("pac-dot")) {
          squares[specialGhost.currentIndex].classList.remove("pac-dot");
          dot = true;
          dotPosition = specialGhost.currentIndex;
        }

        if (specialGhost.isScared) {
          squares[specialGhost.currentIndex].classList.add("scared-ghost");
        }

        checkForGameOver();

        i++;
      }
    }, specialGhost.speed);
  }

  function specialGhostFindPath() {
    let path = findTheShortestPath(
      to2DPosition(specialGhost.currentIndex),
      to2DPosition(pacmanCurrentPosistion),
      layout
    );
    return path;
  }

  function goToScaredPointPath() {
    let scarePointArray = [301, 510, 547];
    let destination = null;
    if (specialGhost.scarePoint === null) {
      destination = scarePointArray[Math.floor(Math.random() * 3)];
      specialGhost.scarePoint = destination;
    } else {
      destination = specialGhost.scarePoint;
    }
    let path = findTheShortestPath(
      to2DPosition(specialGhost.currentIndex),
      to2DPosition(destination),
      layout
    );
    return path;
  }

  function checkScared() {
    if (
      specialGhost.isScared &&
      squares[specialGhost.currentIndex].classList.contains("pac-man")
    ) {
      specialGhost.scarePoint = null;
      squares[specialGhost.currentIndex].classList.remove(
        "special-ghost",
        "ghost",
        "scared-ghost"
      );
      specialGhost.currentIndex = specialGhost.startIndex;
      score += 100;
      scoreBar.innerHTML = score;
      if (specialGhost.isScared) {
        squares[specialGhost.currentIndex].classList.add(
          "special-ghost",
          "ghost",
          "scared-ghost"
        );
      } else {
        squares[specialGhost.currentIndex].classList.add(
          "special-ghost",
          "ghost"
        );
      }
    }
  }

  let chaseTimer = null;
  function chase() {
    chaseTimer = setInterval(() => {
      checkScared();
      let path = null;
      if (specialGhost.isScared) {
        path = goToScaredPointPath();
      } else {
        path = specialGhostFindPath();
      }
      specialGhostMove(path);
    }, 300);
  }
  chase();
  //

  ghosts.forEach((ghost) => {
    squares[ghost.currentIndex].classList.add(ghost.className);
    squares[ghost.currentIndex].classList.add("ghost");
  });

  ghosts.forEach((ghost) => moveGhost(ghost));

  function moveGhost(ghost) {
    const directions = [-1, +1, width, -width];
    let direction = directions[Math.floor(Math.random() * directions.length)];
    let dot = false;
    let dotPosition = null;
    let stepForward = false;
    ghost.timerId = setInterval(function () {
      let addDot = () => {
        if (dot) {
          squares[dotPosition].classList.add("pac-dot");
          dot = false;
          dotPosition = null;
        }
      };
      addDot();

      let checkScared = () => {
        if (
          (ghost.isScared &&
            squares[ghost.currentIndex].classList.contains("pac-man")) ||
          (stepForward &&
            squares[ghost.currentIndex - direction].classList.contains(
              "pac-man"
            ))
        ) {
          addDot();
          squares[ghost.currentIndex].classList.remove(
            ghost.className,
            "ghost",
            "scared-ghost"
          );
          ghost.currentIndex = ghost.startIndex;
          addDot();
          score += 100;
          scoreBar.innerHTML = score;
          squares[ghost.currentIndex].classList.add(ghost.className, "ghost");
        }
      };
      checkScared();

      //if the next squre your ghost is going to go to does not have a ghost and does not have a wall
      if (
        !squares[ghost.currentIndex + direction].classList.contains("ghost") &&
        !squares[ghost.currentIndex + direction].classList.contains("wall") &&
        !squares[ghost.currentIndex + direction].classList.contains(
          "special-ghost"
        )
      ) {
        addDot();

        //remove the ghosts classes
        squares[ghost.currentIndex].classList.remove(ghost.className);
        squares[ghost.currentIndex].classList.remove("ghost", "scared-ghost");

        //move into that space
        ghost.currentIndex += direction;

        addDot();

        if (squares[ghost.currentIndex].classList.contains("pac-dot")) {
          squares[ghost.currentIndex].classList.remove("pac-dot");
          dot = true;
          dotPosition = ghost.currentIndex;
        }

        squares[ghost.currentIndex].classList.add(ghost.className, "ghost");
        stepForward = true;
        //else find a new random direction ot go in
      } else {
        addDot();
        direction = directions[Math.floor(Math.random() * directions.length)];
        stepForward = false;
      }

      //if the ghost is currently scared and pacman is on it
      checkScared();

      //if the ghost is currently scared
      if (ghost.isScared) {
        squares[ghost.currentIndex].classList.add("scared-ghost");
      }

      checkForGameOver();
    }, ghost.speed);
  }

  function checkForGameOver() {
    if (
      squares[pacmanCurrentPosistion].classList.contains("ghost") &&
      !squares[pacmanCurrentPosistion].classList.contains("scared-ghost")
    ) {
      ghosts.forEach((ghost) => clearInterval(ghost.timerId));
      clearInterval(specialGhost.timerId);
      clearInterval(chaseTimer);
      document.removeEventListener("keyup", movePacman);
      setTimeout(function () {
        alert("Game Over");
      }, 500);
    }
  }

  //check for a win - more is when this score is reached
  function checkForWin() {
    if (score >= 500) {
      ghosts.forEach((ghost) => clearInterval(ghost.timerId));
      clearInterval(specialGhost.timerId);
      clearInterval(chaseTimer);
      document.removeEventListener("keyup", movePacman);
      setTimeout(function () {
        alert("You have WON!");
      }, 500);
    }
  }
});
