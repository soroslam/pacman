
function findTheShortestPath(startPoint, endPoint, mazeInput){
let maze = mazeInput;

let openList = [];
let closedList = [];

function node(parent, position) {
    return {
        "parent": parent,
        "position": position,
        "gScore": 0,
        "hScore": 0,
        "fScore": 0
    }
};

let start = startPoint;
let end = endPoint;

let startNode = node(null, start);
let endNode = node(null, end);
let finalNode = null;

openList.push(startNode);
//console.log(openList)

let notFound = true;

while (notFound) {

    let FscoreInOpen = openList.map((e) => {
        return e.fScore
    })

    //console.log(FscoreInOpen);

    let minFscoreInOpen = Math.min(...FscoreInOpen);

    //console.log(minFscoreInOpen);

    let q = openList.find((e) => {
        return e.fScore === minFscoreInOpen
    });

    openList.splice(openList.indexOf(q), 1);

    //console.log(openList);

    let successorPosition = [[0, -1], [0, 1], [-1, 0], [1, 0]];
    let successorList = [];
    for (let i = 0; i < successorPosition.length; i++) {
        let successor = [(q.position[0] + successorPosition[i][0]), (q.position[1] + successorPosition[i][1])];
        if (successor[0] < 0 || successor[0] > maze.length - 1 || successor[1] < 0 || successor[1] > maze[0].length - 1 || maze[successor[0]][successor[1]] === 1) {
            continue;
        }
        let successorNode = node(q, successor);
        successorList.push(successorNode);
        //console.log(successorList)
    };
    for (let i = 0; i < successorList.length; i++) {
        if (successorList[i].position[0] === end[0] && successorList[i].position[1] === end[1]) {
            finalNode = successorList[i];
            notFound = false;
            break;
        };

        successorList[i].gScore = successorList[i].parent.gScore + 1;
        successorList[i].hScore = Math.sqrt((successorList[i].position[0] - end[0]) ** 2 + (successorList[i].position[1] - end[1]) ** 2);
        successorList[i].fScore = successorList[i].gScore + successorList[i].hScore;

        let sccessorInOpen = openList.find((e) => {
            return e.position === successorList[i].position
        });
        //console.log(sccessorInOpen)
        if (sccessorInOpen !== undefined) {
            sccessorInOpen.fScore < successorList[i].fScore;
            continue;
        };

        let sccessorInClose = closedList.find((e) => {
            return e.position === successorList[i].position
        });

        if (sccessorInClose !== undefined) {
            if (sccessorInClose.fScore < successorList[i].fScore) {
                continue;
            }
        };
        openList.push(successorList[i]);
        //console.log(openList)
    };

    closedList.push(q);
}

//console.log(finalNode, 'final');


let pathListFinal = [];
function mypathList(input) {
    if (input.parent !== null) {
        //console.log(input.parent)
        pathListFinal.push(input.position);
        //console.log(input.position)
        //console.log(pathListFinal)
        mypathList(input.parent);
    } else {
        return;
    }
}

mypathList(finalNode);
return pathListFinal.reverse()
}
//let a = findTheShortestPath([0,0], [0,2]);
//console.log(a)